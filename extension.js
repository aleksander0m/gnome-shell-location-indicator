// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

//
//    Copyright (C) 2012 Aleksander Morgado <aleksander@gnu.org>
//
//    This file is part of the 'Location Indicator extension'.
//
//    The 'Location Indicator extension' is free software: you can redistribute
//    it and/or modify it under the terms of the GNU General Public License as
//    published by the Free Software Foundation, either version 2 of the
//    License, or (at your option) any later version.
//
//    The 'Location Indicator extension' is distributed in the hope that it will
//    be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
//    Public License for more details.
//
//    You should have received a copy of the GNU General Public License along
//    with the 'Location Indicator extension'. If not, see
//    http://www.gnu.org/licenses/.
//

const St        = imports.gi.St;
const Main      = imports.ui.main;
const Lang      = imports.lang;
const Gio       = imports.gi.Gio;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Signals   = imports.signals;

const ExtensionUtils  = imports.misc.extensionUtils;
const Me              = ExtensionUtils.getCurrentExtension();
const ModemWatcher    = Me.imports.modemWatcher;
const LocationSection = Me.imports.locationSection;

let indicator = null;

const LocationIndicator = new Lang.Class({
    Name: 'LocationIndicator',
    Extends: PanelMenu.SystemStatusButton,

    _init: function() {
        this.parent('user-home-symbolic', "Location");

	// Visibility of the indicator depends directly on the number of
        // location sources handled. If no location available, indicator is
        // hidden.
	this.actor.visible = false;

        // List of sections
        this._sections = []

        // Setup modem watcher
        this._modem_watcher = new ModemWatcher.ModemWatcher();
        this._modemEnabledId = this._modem_watcher.connect('modem-enabled',  Lang.bind(this, this._ModemEnabled));
        this._modemDisabledId = this._modem_watcher.connect('modem-disabled', Lang.bind(this, this._ModemDisabled));
    },

    _UpdateVisibility: function() {
        // Count number of sections that are to be shown
        let count_visible = 0;
        for (let i = 0; i < this._sections.length; i++) {
            if (this._sections[i].actor.visible)
                count_visible++;
        }

        // Extension icon only visible when there is one or more location sources
        // shown
        this.actor.visible = (count_visible > 0);

        // Section titles and separators only given when more than one device
        // available
        for (let i = 0; i < this._sections.length; i++) {
            this._sections[i].title.actor.visible = (count_visible > 1);
            this._sections[i].separator.actor.visible = (count_visible > 1);
        }
    },

    _ModemEnabled: function(modem_watcher, path) {
        let section = new LocationSection.LocationSection(path);
        // Add the new section
        this._sections.push(section);
        // Leading separator in the section
        this.menu.addMenuItem(section.separator);
        this.menu.addMenuItem(section);
        this._UpdateVisibility();
    },

    _ModemDisabled: function(modem_watcher, path) {
        // Find section and remove
        for (let i = 0; i < this._sections.length; i++) {
            if (this._sections[i].path == path) {
                let section = this._sections[i];
                this._sections.splice(i, 1);
                section.separator.actor.visible = false;
                section.destroy();
                this._UpdateVisibility();
                return;
            }
        }
    },

    destroy: function() {
        for (let i = 0; i < this._sections.length; i++)
            this._sections[i].destroy();
        this._sections = [];

        if (this._modemEnabledId) {
            this._modem_watcher.disconnect(this._modemEnabledId);
            this._modemEnabledId = 0;
        }

        if (this._modemDisabledId) {
            this._modem_watcher.disconnect(this._modemDisabledId);
            this._modemDisabledId = 0;
        }

        this._modem_watcher.destroy();

        this.parent();
    }
});
Signals.addSignalMethods(LocationIndicator.prototype);

function init() {
}

function enable() {
    indicator = new LocationIndicator();
    Main.panel.addToStatusArea('location-indicator', indicator, 0);
}

function disable() {
    indicator.destroy();
    indicator = null;
}
