// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

//
//    Copyright (C) 2012 Aleksander Morgado <aleksander@gnu.org>
//
//    This file is part of the 'Location Indicator extension'.
//
//    The 'Location Indicator extension' is free software: you can redistribute
//    it and/or modify it under the terms of the GNU General Public License as
//    published by the Free Software Foundation, either version 2 of the
//    License, or (at your option) any later version.
//
//    The 'Location Indicator extension' is distributed in the hope that it will
//    be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
//    Public License for more details.
//
//    You should have received a copy of the GNU General Public License along
//    with the 'Location Indicator extension'. If not, see
//    http://www.gnu.org/licenses/.
//

const St       = imports.gi.St;
const Main     = imports.ui.main;
const Lang     = imports.lang;
const Gio      = imports.gi.Gio;
const Signals  = imports.signals;
const Mainloop = imports.mainloop;

// Enumeration of modem states; just the ones we need here.
const ModemState = {
    UNKNOWN: 0,
    ENABLED: 6
}

// The Modem interface, always exposed by all modems exported in ModemManager,
// helps us track the device state changes.
const ModemInterface = <interface name="org.freedesktop.ModemManager1.Modem">
    <property name="State" type="i" access="read" />
    <signal name="StateChanged">
      <arg name="old"    type="i" />
      <arg name="new"    type="i" />
      <arg name="reason" type="u" />
    </signal>
</interface>;
const ModemProxy = Gio.DBusProxy.makeProxyWrapper(ModemInterface);

// The ModemWatcher class, which will notify about modems 'enabled' or
// 'disabled'. It internally takes care of following added/removed modem
// objects as well.
const ModemWatcher = new Lang.Class({
    Name: 'ModemWatcher',

    _init: function() {
        // List of available and enabled modems
        this._modems = [];
        this._enabled_modems = [];

        // Create a new GDBusObjectManagerClient
        this._object_manager = new Gio.DBusObjectManagerClient({
            connection:  Gio.DBus.system,
            name:        'org.freedesktop.ModemManager1',
            object_path: '/org/freedesktop/ModemManager1',
            flags:       Gio.DBusObjectManagerClientFlags.DO_NOT_AUTO_START
        });

        // The GDBusObjectManagerClient is GInitable, so initialize it
        this._object_manager.init(null);

        // Follow changes in the ModemManager1 interface name owner
        this._nameOwnerId = this._object_manager.connect('notify::name-owner', Lang.bind(this, this._ModemManagerNameOwnerChanged));
        this._ModemManagerNameOwnerChanged()

        // Follow added/removed objects
        this._objectAddedId   = this._object_manager.connect('object-added',   Lang.bind(this, this._ModemAdded));
        this._objectRemovedId = this._object_manager.connect('object-removed', Lang.bind(this, this._ModemRemoved));

        // Add initial objects in an idle. We really do need an idle, so that we
        // let the creator of the watcher directly connect to the signals we emit.
        Mainloop.idle_add(Lang.bind(this, function() {
            let modem_object_list = this._object_manager.get_objects();
            for (let i = 0; i < modem_object_list.length; i++)
                this._ModemAdded(this._object_manager, modem_object_list[i]);
            return false;
        }));
    },

    _ModemManagerNameOwnerChanged: function() {
        if (this._object_manager.name_owner)
            global.log('ModemWatcher: ModemManager running');
        else
            global.log('ModemWatcher: ModemManager not running');
    },

    _ModemEnabled: function(modem_proxy) {
        // If found in the list of already enabled modems, just return
        for (let i = 0; i < this._enabled_modems.length; i++)
            if (this._enabled_modems[i].g_object_path ==  modem_proxy.g_object_path)
                return;

        this._enabled_modems.push(modem_proxy);
        global.log('ModemWatcher: [' + modem_proxy.g_object_path + '] enabled');
        this.emit('modem-enabled', modem_proxy.g_object_path);
    },

    _ModemDisabled: function(modem_proxy) {
        // If found in the list of already enabled modems, delete it
        for (let i = 0; i < this._enabled_modems.length; i++) {
            if (this._enabled_modems[i].g_object_path == modem_proxy.g_object_path) {
                this._enabled_modems[i] = null;
                this._enabled_modems.splice(i, 1);
                global.log('ModemWatcher: [' + modem_proxy.g_object_path + '] disabled');
                this.emit('modem-disabled', modem_proxy.g_object_path);
                return;
            }
        }
    },

    _ModemStateUpdated: function(modem_proxy, old_state, new_state) {
        // Just enabled?
        if (old_state < ModemState.ENABLED && new_state >= ModemState.ENABLED) {
            this._ModemEnabled(modem_proxy);
            return;
        }
        // Just disabled?
        if (old_state >= ModemState.ENABLED && new_state < ModemState.ENABLED) {
            this._ModemDisabled(modem_proxy);
            return;
        }
    },

    _ModemAdded: function(manager, object) {
        // If found in the list of existing modems, just return
        for (let i = 0; i < this._modems.length; i++)
            if (this._modems[i].g_object_path == object.g_object_path)
                return;

        // Create modem proxy
        let modem_proxy = new ModemProxy(Gio.DBus.system, 'org.freedesktop.ModemManager1', object.g_object_path);
        modem_proxy.init(null);

        // Add to internal list
        this._modems.push(modem_proxy);
        global.log('ModemWatcher: [' + object.g_object_path + '] added');

        // Follow modem state
        modem_proxy.connectSignal('StateChanged', Lang.bind(this, function(modem_proxy, name, [old_state, new_state, reason]) {
            this._ModemStateUpdated(modem_proxy, old_state, new_state);
        }));
        this._ModemStateUpdated(modem_proxy, ModemState.UNKNOWN, modem_proxy.State);
    },

    _ModemRemoved: function(manager, object) {
        for (let i = 0; i < this._modems.length; i++) {
            // If found in the list of modems, disable and delete it
            if (this._modems[i].g_object_path == object.g_object_path) {
                this._ModemDisabled(this._modems[i]);
                this._modems[i] = null;
                this._modems.splice(i, 1);
                global.log('ModemWatcher: [' + object.g_object_path + '] removed');
                return;
            }
        }
    },

    destroy: function() {
        if (this._nameOwnerId) {
            this._object_manager.disconnect(this._nameOwnerId);
            this._nameOwnerId = 0;
        }

        if (this._objectAddedId) {
            this._object_manager.disconnect(this._objectAddedId);
            this._objectAddedId = 0;
        }

        if (this._objectRemovedId) {
            this._object_manager.disconnect(this._objectRemovedId);
            this._objectRemovedId = 0;
        }

        this._enabled_modems = [];
        this._modems = [];
    }
});
Signals.addSignalMethods(ModemWatcher.prototype);
