// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

//
//    Copyright (C) 2012 Aleksander Morgado <aleksander@gnu.org>
//
//    This file is part of the 'Location Indicator extension'.
//
//    The 'Location Indicator extension' is free software: you can redistribute
//    it and/or modify it under the terms of the GNU General Public License as
//    published by the Free Software Foundation, either version 2 of the
//    License, or (at your option) any later version.
//
//    The 'Location Indicator extension' is distributed in the hope that it will
//    be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
//    Public License for more details.
//
//    You should have received a copy of the GNU General Public License along
//    with the 'Location Indicator extension'. If not, see
//    http://www.gnu.org/licenses/.
//

const St        = imports.gi.St;
const Main      = imports.ui.main;
const Lang      = imports.lang;
const Gio       = imports.gi.Gio;
const Signals   = imports.signals;
const PopupMenu = imports.ui.popupMenu;

// Enumeration for the location sources handled by ModemManager, just the ones
// we want.
const LocationSource = {
    NONE:    0,
    LAC_CI:  1,
    GPS_RAW: 2, // only GPS raw, forget GPS NMEA
    CDMA_BS: 8
};

// The Modem interface, with the description information of the device.
const ModemInterface = <interface name="org.freedesktop.ModemManager1.Modem">
    <property name="Manufacturer" type="s" access="read" />
    <property name="Model"        type="s" access="read" />
</interface>;
const ModemProxy = Gio.DBusProxy.makeProxyWrapper(ModemInterface);

// The Location interface, which gives us the location information updates.
const LocationInterface = <interface name="org.freedesktop.ModemManager1.Modem.Location">
    <method name="Setup">
      <arg name="sources"         type="u" direction="in" />
      <arg name="signal_location" type="b" direction="in" />
    </method>
    <property name="Capabilities" type="u"     access="read" />
    <property name="Enabled"      type="u"     access="read" />
    <property name="Location"     type="a{uv}" access="read" />
</interface>;
const LocationProxy = Gio.DBusProxy.makeProxyWrapper(LocationInterface);

// The LocationMenuItem class, which the information of a single location
// source.
const LocationMenuItem = new Lang.Class({
    Name: 'LocationMenuItem',
    Extends: PopupMenu.PopupBaseMenuItem,

    _init: function(label) {
        this.parent({ reactive: false });

        this._box = new St.BoxLayout({ style_class: 'location-info-box' });
        this._label = new St.Label({ text: label,
                                     style_class: 'location-info-title'});
        this._value = new St.Label({ text: 'unknown',
                                     style_class: 'location-info-value'});
        this.addActor(this._label, {expand: true, align: St.Align.START });
        this.addActor(this._value, {expand: true, align: St.Align.END });
    },

    updateValueString: function(value) {
        if (value == '' || value == '0' || value == '/' || value == '0/0')
            this._value.text = 'unknown';
        else
            this._value.text = value;
    },

    updateValueDegrees: function(degrees_total, positive_char, negative_char) {
        // Convert value to positive
        let positive = (degrees_total >= 0);
        if (!positive)
            degrees_total *= -1.0;

        // Get the base number of degrees.
        let degrees = Math.floor(degrees_total);
        // Convert the remaining degrees to minutes.
        let minutes_total = (degrees_total - degrees) * 60;
        // Get the base number of minutes.
        let minutes = Math.floor(minutes_total);
        // Convert the remainig minutes to seconds.
        let seconds_total = (minutes_total - minutes) * 60;
        // Round the seconds with 5 digits
        let seconds = seconds_total.toFixed(2);
        this._value.text = degrees + '\u00B0' + ' ' + minutes + '\' ' + seconds + '\" ' + (positive ? positive_char : negative_char);
    },

    updateValueFloating: function(value) {
        this._value.text = value.toFixed(2);
    },
});

// The LocationSection class, which contains information about all the location
// information of a given device.
const LocationSection = new Lang.Class({
    Name: 'LocationSection',
    Extends: PopupMenu.PopupMenuSection,

    _init: function(path) {
        this.parent();

        this.path = path;
        this._location_capabilities = 0;

        // Section separator. Not added to the section itself, can be added
        // by the upper layers.
        this.separator = new PopupMenu.PopupSeparatorMenuItem ();

        // Get Vendor/Manufacturer from the Modem inteface.
        let modem_proxy = new ModemProxy(Gio.DBus.system, 'org.freedesktop.ModemManager1', path);
        modem_proxy.init(null);
        let title_str = modem_proxy.Model + ' (' + modem_proxy.Manufacturer + ')';
        this.title = new PopupMenu.PopupMenuItem(title_str, { style_class: 'location-section-title',
                                                              reactive: false} );
        this.addMenuItem(this.title);

        // Setup proxy for the Location inteface.
        this._location_proxy = new LocationProxy(Gio.DBus.system, 'org.freedesktop.ModemManager1', path);
        this._location_proxy.init(null);

        // If no location capabilities, hidden section, and nothing else to do
        if (!this._location_proxy.Capabilities) {
            this.actor.visible = false;
            global.log ('Location: [' + path + '] has no location capabilities');
            return;
        }

        // If at least one location capability, show section
        this.actor.visible = true;
        this._location_capabilities = this._location_proxy.Capabilities;

        // 3GPP location
        if (this._location_capabilities & LocationSource.LAC_CI) {
            global.log ('Location: [' + path + '] has 3GPP (LAC/CI) location capabilities');
            this._3gpp_switch = new PopupMenu.PopupSwitchMenuItem("3GPP location", false, { style_class: 'location-section-title' });
            this._3gpp_switch.connect('toggled', Lang.bind(this, function() {
                this._setupEnabledCapabilities(this._3gpp_switch.state, LocationSource.LAC_CI);
            }));

            this.addMenuItem(this._3gpp_switch);

            this._3gpp_mccmnc = new LocationMenuItem('MCC/MNC:');
            this.addMenuItem(this._3gpp_mccmnc);
            this._3gpp_lac = new LocationMenuItem('Location area code:');
            this.addMenuItem(this._3gpp_lac);
            this._3gpp_ci = new LocationMenuItem('Cell ID:');
            this.addMenuItem(this._3gpp_ci);
        }

        // GPS location
        if (this._location_capabilities & LocationSource.GPS_RAW) {
            global.log ('Location: [' + path + '] has GPS location capabilities');
            this._gps_switch = new PopupMenu.PopupSwitchMenuItem("GPS location", false, { style_class: 'location-section-title' });
            this._gps_switch.connect('toggled', Lang.bind(this, function() {
                this._setupEnabledCapabilities(this._gps_switch.state, LocationSource.GPS_RAW);
            }));

            this.addMenuItem(this._gps_switch);

            this._gps_latitude = new LocationMenuItem('Latitude:');
            this.addMenuItem(this._gps_latitude);
            this._gps_longitude = new LocationMenuItem('Longitude:');
            this.addMenuItem(this._gps_longitude);
            this._gps_altitude = new LocationMenuItem('Altitude:');
            this.addMenuItem(this._gps_altitude);
        }

        // CDMA BS location
        if (this._location_capabilities & LocationSource.CDMA_BS) {
            global.log ('Location: [' + path + '] has 3GPP2 (CDMA BS) location capabilities');
            this._cdma_switch = new PopupMenu.PopupSwitchMenuItem("CDMA location", false, { style_class: 'location-section-title' });
            this._cdma_switch.connect('toggled', Lang.bind(this, function() {
                this._setupEnabledCapabilities(this._cdma_switch.state, LocationSource.CDMA_BS);
            }));

            this.addMenuItem(this.cdma_switch);

            this._cdma_latitude = new LocationMenuItem('Latitude:');
            this.addMenuItem(this._cdma_latitude);
            this._cdma_longitude = new LocationMenuItem('Longitude:');
            this.addMenuItem(this._cdma_longitude);
        }

        // Enabled capabilities may change
        this._location_proxy.connect('g-properties-changed', Lang.bind(this, function(proxy, properties) {
            let unpacked = properties.deep_unpack();
            if ('Enabled' in unpacked)
                this._enabledCapabilitiesUpdated(unpacked['Enabled'].deep_unpack());
            if ('Location' in unpacked)
                this._locationUpdated(unpacked['Location'].deep_unpack());
        }));

        // Ensure we setup capabilities reporting in the Location property. We
        // need to re-enable those capabilities exposed initially as enabled,
        // so that the 'notify-location' is set to 'true'.
        this._setupEnabledCapabilities(true, this._location_proxy.Enabled);
    },

    _setupEnabledCapabilities: function(state, source) {
        let new_capabilities = this._location_proxy.Enabled;
        if (state)
            new_capabilities |= source;
        else
            new_capabilities &= ~source;

        this._location_proxy.SetupRemote(new_capabilities, true, Lang.bind(this, function(){
            this._enabledCapabilitiesUpdated(this._location_proxy.Enabled);
            this._locationUpdated(this._location_proxy.Location);
        }));
    },

    _enabledCapabilitiesUpdated: function(enabled_capabilities) {
        if (this._3gpp_switch) {
            let visible = enabled_capabilities & LocationSource.LAC_CI;
            this._3gpp_mccmnc.actor.visible = visible;
            this._3gpp_lac.actor.visible = visible;
            this._3gpp_ci.actor.visible = visible;
            this._3gpp_switch.setToggleState(visible);
        }

        if (this._gps_switch) {
            let visible = enabled_capabilities & LocationSource.GPS_RAW;
            this._gps_latitude.actor.visible = visible;
            this._gps_longitude.actor.visible = visible;
            this._gps_altitude.actor.visible = visible;
            this._gps_switch.setToggleState(visible);
        }

        if (this._cdma_switch) {
            let visible = enabled_capabilities & LocationSource.CDMA_BS;
            this._cdma_latitude.actor.visible = visible;
            this._cdma_longitude.actor.visible = visible;
            this._cdma_switch.setToggleState(visible);
        }
    },

    _locationUpdated: function(new_location) {
        if (LocationSource.LAC_CI in new_location) {
            // Expected string format: 'MCC,MNC,LAC,CI'
            let location = new_location[LocationSource.LAC_CI].deep_unpack();
            let split = location.split(',');

            if (this._3gpp_mccmnc)
                this._3gpp_mccmnc.updateValueString(split[0] + '/' + split[1]);
            if (this._3gpp_lac)
                this._3gpp_lac.updateValueString(split[2]);
            if (this._3gpp_ci)
                this._3gpp_ci.updateValueString(split[3]);
        }

        if (LocationSource.GPS_RAW in new_location) {
            let location = new_location[LocationSource.GPS_RAW].deep_unpack();

            if (this._gps_latitude && 'latitude' in location)
                this._gps_latitude.updateValueDegrees(location['latitude'].deep_unpack(), 'N', 'S')
            else
                this._gps_latitude.updateValueString('')

            if (this._gps_longitude && 'longitude' in location)
                this._gps_longitude.updateValueDegrees(location['longitude'].deep_unpack(), 'E', 'W')
            else
                this._gps_longitude.updateValueString('')

            if (this._gps_altitude && 'altitude' in location)
                this._gps_altitude.updateValueFloating(location['altitude'].deep_unpack())
            else
                this._gps_altitude.updateValueString('')
        }

        if (LocationSource.CDMA_BS in new_location) {
            let location = new_location[LocationSource.CDMA_BS].deep_unpack();

            if (this._cdma_latitude && 'latitude' in location)
                this._cdma_latitude.updateValueDegrees(location['latitude'].deep_unpack(), 'N', 'S')
            else
                this._cdma_latitude.updateValueString('')

            if (this._cdma_longitude && 'longitude' in location)
                this._cdma_longitude.updateValueDegrees (location['longitude'].deep_unpack(), 'E', 'W')
            else
                this._cdma_longitude.updateValueString('')
        }
    },
});
Signals.addSignalMethods(LocationSection.prototype);
